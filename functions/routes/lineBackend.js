var express = require('express');
var router = express.Router();
var request = require('request');
const crypto = require('crypto');

const config = require("../configs")

router.post('/', (req, res, next) => {
    // Validate signature
    // console.log(`Header v.8: ${req.get('X-Line-Signature')}`);
    // console.log(`Request: ${String(req.body)}`);
    // const signature = crypto
    //     .createHmac('SHA256', config.LineConfig.channelSecret)
    //     .update(req).digest('base64');

    // if (req.get('X-Line-Signature') !== signature) {
    //     res.status(401);
    //     res.send("Invalid Signature");
    //     return;
    // }

    Promise
        .all(req.body.events.map(handleEvent))
        .then((result) => res.json(result))
        .catch(
            console.log(req)
        );
})

function handleEvent(event) {
    if (event.type !== 'message' || event.message.type !== 'text') {
        return Promise.resolve(null);
    }
    // console.log(`replyToken: ${event.replyToken}`);
    const options = {
        url: 'https://api.line.me/v2/bot/message/reply',
        headers: {
            'Authorization': `Bearer ${config.LineConfig.channelAccessToken}`
        },
        method: 'POST',
        json: {
            'replyToken': event.replyToken,
            'messages':
                [{
                    type: 'text',
                    text: event.message.text
                }]
        }
    }
    // console.log(`options: ${options}`);
    request(options, function (err, res, body) {
        console.log(err);
        // console.log(`body: ${body}`);
        return Promise.resolve(null);
    });
}

module.exports = router;
