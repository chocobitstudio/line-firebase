var express = require('express');
var router = express.Router();

// Healthcheck
router.get('/', function (req, res, next) {
    res.send('Hello Express in Firebase');
});

router.post('/', function (req, res, next) {
    res.send(req.body.message);
});

module.exports = router;