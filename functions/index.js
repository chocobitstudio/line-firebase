const functions = require("firebase-functions")
const express = require("express")

const lineBackend = require("./routes/lineBackend")
const healthCheck = require("./routes/healthCheck")

// Line's
const config = require ("./configs")

/* Express */
const appLine = express()
appLine.use('/', lineBackend)

const lineAPI = functions.https.onRequest(appLine)

/* Express with CORS */
const appHealthCheck = express()
appHealthCheck.use('/', healthCheck)

const healthcheckAPI = functions.https.onRequest(appHealthCheck)

module.exports = {
    lineAPI,
    healthcheckAPI
}